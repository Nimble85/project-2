## Разобратся с gitlab-ci ##	


- [x] *1. Установить gitlab через докер (мы используем сборку https://github.com/sameersbn/docker-gitlab)*		



> Установка:
> apt-get update

> apt-get install git ansible docker.io -y | apt-get install docker-compose -y

> docker run -d -p 80:80 -p 23:22 --name gitlab gitlab/gitlab-ce:latest

> docker exec -it gitlab /bin/bash




- [ ] *2. Поднять 2а gitlab runner-а*
	

- [ ] *3. Взять любой небольшой популярный php проект(c автотестами, желательно которые строят отчёт покрытия в html формате), добавить его в гитлаб и настроить gitlab-ci:* 			
```
    - добавить 2 ветки в проект master develop
    - при пуше в девелоп  gitlab-ci должен запускать авто тесты
    - отчёт покрытия должен открыватся в gitlab Pages и доступен по урлу	
```	

	Info: для сборки использовать docker-compose 								
	Дополнительно: для настройки  сервера можно использовать Ansible, Terraform) ,что бы мы легко могли развернуть наш гитлаб на любом облаке							
	